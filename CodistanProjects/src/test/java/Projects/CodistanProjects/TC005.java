package Projects.CodistanProjects;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.Test;

public class TC005 {
@Test 
public void TC005() {
	//Given an empty array of size 10	When we add, 3, 5, 1, 7, 9 and print the items in the reverse order	Then we should see 9, 7, 1, 5, 3		
    List<Integer> list = Arrays.asList(3,5,1,7,9);
    System.out.println(list);
    Collections.reverse(list);
    System.out.println(list);
}
}
