package Projects.CodistanProjects;

import org.testng.annotations.Test;

public class TC014 {
	@Test
	public void TC014() {
		// Given a String as "Hello World" When change all the "o" character with "a"
		// Then you will get "Hella Warld"
		String str1 = "Hello World";
		str1 = str1.replace('o', 'a');
		System.out.println(str1);

}
}
